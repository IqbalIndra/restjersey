package org.tester.restjersey.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.tester.restjersey.model.Country;
import org.tester.restjersey.service.CountryService;
import org.tester.restjersey.service.CountryServiceImpl;

@Path("/countries")
public class CountryController {

	CountryService countryService;
	
	public CountryController() {
		countryService = new CountryServiceImpl();
		
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Country> getCountries(){
		return countryService.getAllCountries();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Country getCountryById(@PathParam("id")int id){
		return countryService.getCountryById(id);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Country addCountry(Country country){
		return countryService.saveCountry(country);
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Country updateCountry(Country country){
		return countryService.updateCountry(country);
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public void deleteCountry(@PathParam("id")int id){
		countryService.deleteCountry(id);
	}
	
}

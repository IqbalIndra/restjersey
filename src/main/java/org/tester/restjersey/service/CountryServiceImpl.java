/**
 * 
 */
package org.tester.restjersey.service;

import java.util.ArrayList;
import java.util.List;

import org.tester.restjersey.model.Country;

/**
 * @author LENOVO G40-70
 *
 */
public class CountryServiceImpl implements CountryService {
	private static List<Country> countries;
	{
		countries = new ArrayList<>();
		for (int i = 1; i <= 5; i++) {
			Country country = new Country();
			country.setId(i);
			country.setCountryName("Country " + i);
			country.setPopulation(500 + (i * 20));
			countries.add(country);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.tester.restjersey.service.CountryService#getAllCountries()
	 */
	@Override
	public List<Country> getAllCountries() {
		// TODO Auto-generated method stub
		return countries;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.tester.restjersey.service.CountryService#getCountryById(int)
	 */
	@Override
	public Country getCountryById(int id) {
		// TODO Auto-generated method stub
		for (Country country : countries) {
			if (country.getId() == id)
				return country;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.tester.restjersey.service.CountryService#saveCountry(org.tester.
	 * restjersey.model.Country)
	 */
	@Override
	public Country saveCountry(Country country) {
		// TODO Auto-generated method stub
		if(country != null){
			Country checkCountry = getCountryById(country.getId());
			if(checkCountry == null)
				countries.add(country);
			else
				updateCountry(country);
		}
		return country;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.tester.restjersey.service.CountryService#updateCountry(org.tester.
	 * restjersey.model.Country)
	 */
	@Override
	public Country updateCountry(Country country) {
		// TODO Auto-generated method stub
		if (country != null) {
			int index = countries.indexOf(country);
			countries.set(index, country);
		}
		return country;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.tester.restjersey.service.CountryService#deleteCountry(int)
	 */
	@Override
	public void deleteCountry(int id) {
		// TODO Auto-generated method stub
		Country country = getCountryById(id);
		countries.remove(country);
	}

}

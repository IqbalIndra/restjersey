package org.tester.restjersey.service;

import java.util.List;

import org.tester.restjersey.model.Country;

public interface CountryService {
	List<Country> getAllCountries();
	Country getCountryById(int id);
	Country saveCountry(Country country);
	Country updateCountry(Country country);
	void deleteCountry(int id);
	
}
